<?php
/**
 * User: Bakhtiyor Ruziev
 * Date: 21.03.15
 * Time: 15:35
 */

namespace Asad;



/**
 * Class NumToWord
 * @package Asad
 */
class NumToWord
{

    /**
     * @var array
     */
    private $digitName = [
        "",
        "як",
        "ду",
        "се",
        "чор",
        "панч",
        "шаш",
        "хафт",
        "хашт",
        "нух",
        "дах",
        "ездах",
        "дувоздах",
        "сенздах",
        "чордах",
        "понздах",
        "шонздах",
        "хабдах",
        "хаждах",
        "нуздах",
    ];
    /**
     * @var array
     */
    private $tenDigitName = [
        "",
        "дах",
        "бист",
        "си",
        "чил",
        "панчох",
        "шаст",
        "хафтод",
        "хаштод",
        "навад",
    ];

    /**
     * @var array
     */
    private $specialName = [
        "",
        'хазор',
        'миллион',
        'миллиард',
        'трилиард',
    ];

    /**
     * @var int|string
     */
    public $number;
    /**
     * @var int
     */
    public $intNumber;

    function __construct($number)
    {
        if (!is_numeric($number)) {
            throw new \Exception("It's not number.");
        }
        $this->number = $number;
        $this->intNumber = intval($number);
    }

    /**
     * @param $number
     * @return string
     */
    private function lessThousand($number)
    {
        $postfix = '';
        if ($number == 100) {
            return 'сад';
        }
        if ($number % 100 < 20) {
            $current = $this->digitName[$number % 100];
            $number = intval($number / 100);
        } else {
            if (($number % 10) != 0) {
                $postfix = 'у';
            }
            $current = $this->digitName[$number % 10];
            $number = intval($number / 10);
            $current = $this->tenDigitName[$number % 10] . $postfix . ' ' . $current;

            $number = intval($number / 10);

        }
        if ($number == 0) {
            return $current;
        }

        if ($current == '') {
            return $this->digitName[$number] . "сад";
        }

        return $this->digitName[$number] . "саду " . $current;
    }

    /**
     * @return string
     */
    private function convert()
    {
        $number = $this->number;
        $current = '';

        if ($this->number == 0) {
            return 'сифр';
        }

        $place = 0;

        do {
            $n = $number % 1000;
            if ($n != 0) {
                $s = $this->lessThousand($n);

                if ($place == 1 && ($n % 10) != 0) {
                    $specialName = $this->specialName[$place] . 'у';
                } else {
                    $specialName = $this->specialName[$place];
                }
                $current = $s . ' ' . $specialName . ' ' . $current;
            }
            $place++;

            $number = intval($number / 1000);

        } while ($number > 0);


        return $current;
    }

    /**
     * @return string
     */
    public function toWord()
    {
        return $this->convert();
    }


}